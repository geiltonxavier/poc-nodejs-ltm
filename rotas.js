module.exports = {


  getUsuarios: function(req, res){

      res.json({message: "rota para GET do /usuarios"})  
    
  },
  getParticipantes: function(req, res){
     
    var sql = require("mssql");  

   //local
    // var config = {
    //     server: '10.102.0.16', 
    //     database: 'DB_Nestle_Venca' ,
    //     user: 'temp.nestle',
    //     password: 'ltm@12345',
    //     port: 1433
    // };
  //azure
    var config = {
              user: 'aplic.BRFmsa',
              password: 'SAYZ@cpLZV2oa9',
              server: 'brfmaisdev.database.windows.net', 
              options: {encrypt: true, database: 'DB_LTM_WebForms'}  
        };
    
    sql.connect(config, function (err) {
    
        if (err) console.log(err); 

        var request = new sql.Request();          
        
        request.query('select top 10 * from Tb_Participante', function (err, recordset) {            
           
            if (err) console.log(err)
            
            res.json(recordset)
            
        });
    });
  },
  getFake: function(req,res){

      var fakeObj = [
                {
                    "Id": 1,
                    "CodigoExterno": "859315",
                    "Nome": "Marcelo+Mazini+Bomfim+Junior",
                    "IdCliente": 6,
                    "Ativo": true,
                    "DtInclusao": "2016-07-06T21:15:28.880Z",
                    "DtAlteracao": null,
                    "IdMigracao": null
                },
                {
                    "Id": 2,
                    "CodigoExterno": "1",
                    "Nome": "William",
                    "IdCliente": 1,
                    "Ativo": true,
                    "DtInclusao": "2016-11-03T21:37:24.596Z",
                    "DtAlteracao": null,
                    "IdMigracao": null
                },
                {
                    "Id": 3,
                    "CodigoExterno": "1",
                    "Nome": "Trade Hypermarcas",
                    "IdCliente": 8,
                    "Ativo": true,
                    "DtInclusao": "2016-12-07T21:04:55.793Z",
                    "DtAlteracao": null,
                    "IdMigracao": null
                },
                {
                    "Id": 4,
                    "CodigoExterno": "0",
                    "Nome": "Will",
                    "IdCliente": 1,
                    "Ativo": true,
                    "DtInclusao": "2016-12-07T21:08:45.343Z",
                    "DtAlteracao": null,
                    "IdMigracao": null
                },
                {
                    "Id": 5,
                    "CodigoExterno": "5610",
                    "Nome": "Marketing Hypermarcas",
                    "IdCliente": 8,
                    "Ativo": true,
                    "DtInclusao": "2016-12-13T22:07:23.026Z",
                    "DtAlteracao": null,
                    "IdMigracao": null
                },
                {
                    "Id": 6,
                    "CodigoExterno": "8390",
                    "Nome": "Gerente Hypermarcas",
                    "IdCliente": 8,
                    "Ativo": true,
                    "DtInclusao": "2016-12-13T22:10:01.950Z",
                    "DtAlteracao": null,
                    "IdMigracao": null
                },
                {
                    "Id": 7,
                    "CodigoExterno": "2",
                    "Nome": "Trade Hypermarcas",
                    "IdCliente": 8,
                    "Ativo": true,
                    "DtInclusao": "2016-12-16T20:14:35.490Z",
                    "DtAlteracao": null,
                    "IdMigracao": null
                },
                {
                    "Id": 8,
                    "CodigoExterno": "3588",
                    "Nome": "Trade Hypermarcas",
                    "IdCliente": 8,
                    "Ativo": true,
                    "DtInclusao": "2016-12-16T20:16:47.990Z",
                    "DtAlteracao": null,
                    "IdMigracao": null
                },
                {
                    "Id": 9,
                    "CodigoExterno": "3",
                    "Nome": "Hyper 3",
                    "IdCliente": 8,
                    "Ativo": true,
                    "DtInclusao": "2016-12-19T18:49:54.256Z",
                    "DtAlteracao": null,
                    "IdMigracao": null
                },
                {
                    "Id": 10,
                    "CodigoExterno": "4",
                    "Nome": "Hyper 4",
                    "IdCliente": 8,
                    "Ativo": true,
                    "DtInclusao": "2016-12-19T18:50:10.243Z",
                    "DtAlteracao": null,
                    "IdMigracao": null
                }
        ]
     res.json(fakeObj)
  },

  postUsuarios: require('./controllerCriaUsuario'),
  postApuracao: require('./controllerCriaEfetivo'),
  getApuracao: function(req, res){
      
     var apuracao = require('./ModelApuracao');
     
      apuracao.find({}, function(err, docs) {
                if (!err){ 
                    res.json(docs)  
                    // process.exit();
                } else {throw err;}
           }).limit(10000);     
    
  },

  getApuracaoById: function(req, res){
      
     var apuracao = require('./ModelApuracao');
     
      apuracao.find({}, function(err, docs) {
                if (!err){ 
                    res.json(docs)  
                    // process.exit();
                } else {throw err;}
           }).where('id').equals(req.params.id)   
    
  },

getApuracaoMapReduce: function(req, res){
    var apuracao = require('./ModelApuracao');
    
    //     var pipeline = [
    //     {
    //         "$group": {
    //             "_id": "$Canal",
    //             "count": { "$sum": 1 }
    //         }
    //     }
    // ];

    // apuracao.aggregate(pipeline, function (err, res) {
    //     if(err) throw err;
    //     res.json(res)
    // });

     var o = {},
     self = this;
    o.map = function () {
        emit(this.Canal, 1)
    };
    o.reduce = function (k, vals) {
        return vals.length
    };

    apuracao.mapReduce(o, function (err, res) {
        if(err) throw err;
        res.json(res)
    });



  },
  login: require('./controllerLogin')
}