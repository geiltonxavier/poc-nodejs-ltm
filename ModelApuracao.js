var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
//1
var ApuracaoSchema = new mongoose.Schema({
Company: {
    type: String   
  },
Sales_Employee_Key: {
    type: String   
  },
Sales_Employee_Name: {
    type: String   
  },
Month: {
    type: String   
  },
Customer_Key: {
    type: String   
  },
Customer_Name: {
    type: String   
  },
Regional: {
    type: String   
  },
Filial: {
    type: String   
  },
Canal: {
    type: String   
  },
Material_Key: {
    type: String   
  },
Material_Name: {
    type: String   
  },
Volume: {
    type: String   
  },
Base_unit: {
    type: String   
  },
NNS: {
    type: String   
  },
idArquivoEfetivoValidar: {
    type: String   
  },
idArquivoEfetivoValidarItem: {
    type: String   
  },
Broker: {
    type: String   
  },
Planta: {
    type: String   
  },
Cust_Level6_Key: {
    type: String   
  },
Cust_Level6_Name: {
    type: String   
  },
id: {
    type: String   
  },
Month_Erro: {
    type: String   
  },
Regional_Erro: {
    type: String   
  },
Filial_Erro: {
    type: String   
  },
Canal_Erro: {
    type: String   
  },
Broker_Erro: {
    type: String   
  },
Company_Erro: {
    type: String   
  },
Volume_Erro: {
    type: String   
  },
NNS_Erro: {
    type: String   
  },
FLAG_ERRO: {
    type: String   
  },
NomeArquivo: {
    type: String   
  },
idFilial: {
    type: String   
  },
idRegional: {
    type: String   
  },
idParticipante: {
    type: String   
  },
Mes: {
    type: String   
  },
Ano: {
    type: String   
  },
idPDV: {
    type: String   
  },
idCanal: {
    type: String   
  },
idProdutoVenca: {
    type: String   
  },
CodSold: {
    type: String   
  },
PlantaBroker: {
    type: String   
  },
NF: {
    type: String   
  }
});
module.exports = mongoose.model('Apuracao', ApuracaoSchema)