var fs = require('fs');
var https = require('https');
var http  = require('http');
var express = require('express')
  app = express(),
  bodyParser = require('body-parser'),
  mongoose = require('mongoose'),
  jwt = require('jwt-simple');

 //certificados para https
  var options = {
   key  : fs.readFileSync('server.key'),
   cert : fs.readFileSync('server.crt')
  };

  //mongo lab  
  var db = 'mongodb://ltm:ltm@ds159998.mlab.com:59998/ltm'
  
  //local
  // var db = 'mongodb://127.0.0.1:27017/ltm';

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  var port = process.env.PORT || 8080;
  var router = express.Router();
  app.use('/api', router);

  /*Aqui criaremos as rotas*/
  var rotas = require('./rotas');

  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });


  router.route('/usuarios')
    .get(require('./validarJWT'), rotas.getUsuarios)  //alteramos esta
    .post(rotas.postUsuarios); 

  router.route('/participantes')
    .get(require('./validarJWT'), rotas.getParticipantes)

  router.route('/apuracao')
    .get(rotas.getApuracao) 
    .post(rotas.postApuracao) 

    router.route('/apuracaopost')
    .post(rotas.getApuracao)  

    router.route('/apuracao_por_id/:id')
    .get(require('./validarJWT'),rotas.getApuracaoById)  

    router.route('/getapuracao_mapreduce')
    .get(rotas.getApuracaoMapReduce) 
      

	router.route('/login')
    .post(rotas.login);

  router.route('/fake')
    .get(rotas.getFake)    

  mongoose.Promise = global.Promise;
  mongoose.connect(db);

   app.listen(port);

  // https.createServer(options, app).listen(port, function () {
  //  console.log('Started https');
  // });

  console.log('conectado a porta ' + port);